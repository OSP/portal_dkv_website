$(document).on('turbolinks:load', function(){
    
    // Keperluan Password
    var password_strenght = 0;

    $('#user_password').on('keyup', function() {
        var password_length = $('#user_password').val().length;
        if(password_length >= 2){
            $('.secret_field').removeClass('hidden_field');
            switch (true){
                case (password_length < 6 && password_length >= 0):
                    password_strenght = 20;
                    break;
                case (password_length < 8 && password_length >= 6):
                    password_strenght = 50;
                    break;
                case (password_length < 10 && password_length >= 8):
                    password_strenght = 75;
                    break;
                case (password_length < 14 && password_length >= 10):
                    password_strenght = 85;
                    break;
                case (password_length < 16 && password_length >= 16):
                    password_strenght = 100;
                    break;
                default: 
                    password_strenght = 0;
                    break;
            }
        }
        else{
            $('.secret_field').addClass('hidden_field');
            password_strenght = 0;
        }
        $('.progress').progress({
            percent: password_strenght
        });
    });

    // Redirect
    $('#register_button').on('click', function() {
        var email = $("#user_email").val();
        var domain = email.substring(email.lastIndexOf("@") +1);
        var open = window.open('https://' + domain, '_blank');

        if (open) {
            //Browser has allowed it to be opened
            open.focus();
        }
    });


    // Validasi Front End
    $('.ui.form')
    .form({
        fields: {
            nama_lengkap: {
                identifier: 'nama_lengkap',
                rules: [
                {
                    type   : 'empty',
                    prompt : 'Nama lengkap harus diisi'
                }
                ]
            },
            user_email: {
                identifier: 'user_email',
                rules: [
                {
                    type   : 'email',
                    prompt : 'Email tidak valid'
                }
                ]
            },
            user_password: {
                identifier: 'user_password',
                rules: [
                {
                    type   : 'minLength[6]',
                    prompt : 'Masukan minimal 6 karakter kata sandi'
                }
                ]
            },
            user_password_confirmation: {
                identifier: 'user_password_confirmation',
                rules: [
                {
                    type   : 'match[user_password]',
                    prompt : 'Kata sandi tidak sama'
                }
                ]
            }
        }
    })
    ;
})