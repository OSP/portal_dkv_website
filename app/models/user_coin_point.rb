class UserCoinPoint < ApplicationRecord
    belongs_to :user
    has_many :user_coin_histories
    has_many :user_point_histories
end