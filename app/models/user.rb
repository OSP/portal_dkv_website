class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :validatable, :trackable
          
  has_one :user_biography
  has_one :user_coin_point
  has_many :user_mail_histories
  has_many :user_coin_histories, through: :usr_coin_point
  has_many :user_point_histories, through: :usr_coin_point
  has_many :quotes
  has_many :user_interests
  has_many :videos
  has_many :articles
  has_many :notifications
  has_many :user_video_view_histories
  has_many :user_article_view_histories

  accepts_nested_attributes_for :user_coin_point, allow_destroy: true
  accepts_nested_attributes_for :user_biography, allow_destroy: true
  after_create :create_user_coin_point
  after_create :create_user_biography

  def generate_jwt
    JWT.encode({ id: id,
                exp: 60.days.from_now.to_i },
               Rails.application.secrets.secret_key_base)
  end

  def self.generate_username(user_real_name)
    user_name = user_real_name.delete(' ').slice(0..3)
    user_name += Time.now.strftime("%y%m%S").slice(1..6) + rand(10..99).to_s
    if User.where("username = '#{user_name}'").present?
      return user_name += rand(1..99).to_s
    else
      return user_name
    end
  end
end