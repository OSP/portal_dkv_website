class CruisesController < ApplicationController

    def index
        respond_to do |format|
            format.html
        end
    end

    private
    def video_all
        @video_list = Video.joins(:user, :category).select("videos.id, videos.name as nama_video, videos.url as alamat_video, users.username as nama_kreator, categories.name as kategori, videos.description as deskripsi_video, videos.created_at as tanggal_unggah, videos.view_count as dilihat").where("videos.validasi = 3")
    end
    def top_video
        video_all
        @top_video = @video_list.order(dilihat: :desc).first(10)
    end
    def video_by_topic
        video_all
        @category_list = Category.select("id, category_name").all
        @top_video = @video_list.where(kategori: "a").first(10)
    end
end