class BasicInformationsController < ApplicationController

    def email_confirmation_page
        byebug
        respond_to do |format|
          format.html
        end
    end

    def faq
        respond_to do |format|
            format.html
        end
    end

    def about_us
        respond_to do |format|
            format.html
        end
    end

    def privacy_policy
        respond_to do |format|
            format.html
        end
    end

    def how_it_work
        respond_to do |format|
            format.html
        end
    end

end