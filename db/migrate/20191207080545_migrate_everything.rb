class MigrateEverything < ActiveRecord::Migration[6.0]
  def change
    create_table :user_mail_histories do |t|
      t.integer :user_id
      t.string :email
      t.timestamps
    end

    create_table :user_coin_points do |t|
      t.integer :user_id
      t.integer :coin
      t.integer :point
      t.timestamps
    end

    create_table :user_coin_histories do |t|
      t.integer :user_coin_point_id
      t.integer :coin
      t.string :type
      t.timestamps
    end

    create_table :user_point_histories do |t|
      t.integer :user_coin_point_id
      t.integer :point
      t.string :type
      t.timestamps
    end
    create_table :user_biographies do |t|
      t.integer :user_id
      t.string :name
      t.string :description
      t.string :avatar
      t.timestamps
    end
    create_table :user_video_view_histories do |t|
      t.integer :user_id
      t.integer :video_id
      t.timestamps
    end
    create_table :user_article_view_histories do |t|
      t.integer :user_id
      t.integer :article_id
      t.timestamps
    end
    create_table :videos do |t|
      t.integer :user_id
      t.string :name
      t.string :url
      t.string :description
      t.string :category_id
      t.timestamps
    end
    create_table :articles do |t|
      t.integer :user_id
      t.string :title
      t.string :description
      t.string :category_id
      t.timestamps
    end
    create_table :quotes do |t|
      t.string :quote
      t.integer :user_id
      t.timestamps
    end
    create_table :user_interests do |t|
      t.integer :user_id
      t.integer :category_id
      t.timestamps
    end
    create_table :categories do |t|
      t.string :name
      t.integer :parent_id
      t.timestamps
    end

      change_column :videos ,:category_id, 'integer USING CAST(category_id AS integer)'
      change_column :articles ,:category_id, 'integer USING CAST(category_id AS integer)'
      add_foreign_key :user_mail_histories, :users
      add_foreign_key :user_coin_points, :users
      add_foreign_key :user_coin_histories, :user_coin_points
      add_foreign_key :user_point_histories, :user_coin_points
      add_foreign_key :user_biographies, :users
      add_foreign_key :quotes, :users
      add_foreign_key :user_interests, :users
  
      add_foreign_key :user_video_view_histories, :users
      add_foreign_key :user_video_view_histories, :videos
      add_foreign_key :user_article_view_histories, :users
      add_foreign_key :user_article_view_histories, :articles
      add_foreign_key :videos, :users
      add_foreign_key :videos, :categories
      add_foreign_key :articles, :users
      add_foreign_key :articles, :categories

      create_table :jwt_blacklist do |t|
        t.string :jti, null: false
      end
      add_index :jwt_blacklist, :jti

      change_column_default :user_coin_points, :coin, to: 0
      change_column_default :user_coin_points, :point, to: 0

      create_table :notifications do |t|
        t.integer :user_id
        t.integer :notification_type_id
        t.string :reference_id
        t.string :message
        t.boolean :seen
        t.timestamps
      end

      create_table :notification_types do |t|
        t.string :name_type
        t.timestamps
      end
      add_foreign_key :notifications, :users
      add_foreign_key :notifications, :notification_types

      add_column :notifications, :url, :string
      add_column :notifications, :thumbnail, :string
      add_column :videos, :thumbnail, :string

      add_column :users, :username_changed, :boolean
      add_column :users, :dewa, :boolean

      add_column :videos, :validate, :integer
      add_column :videos, :validation_date, :datetime
      add_column :videos, :validation_by, :string
      add_column :videos, :validation_message, :string

      rename_column :videos, :validate, :validasi

      add_column :user_biographies, :gender, :int, :null => false, :default => 0
  end
end
