Rails.application.routes.draw do
  devise_for :users,
  path: '',
  path_names: {
    sign_in: 'masuk',
    sign_out: 'keluar',
    sign_up: 'daftar'
  },
  controllers: {
      confirmations: 'users/confirmations',
      #omniauth_callbacks: 'users/omniauth_callbacks',
      passwords: 'users/passwords',
      unlocks: 'users/unlocks',
      sessions: 'users/sessions',
      registrations: 'users/registrations'
  }
  
  root to: 'houses#index', only: [:index]
  
  resources :trends, :path => "populer", only: [:index]
  resources :cruises, :path => "jelajah", only: [:index]
  resources :inputs, :path => "unggah", except: :destroy
  
  # Informasi Dasar
  get 'konfirmasi', to: 'basic_informations#email_confirmation_page'
  get 'tentang', to: 'basic_informations#about_us'
  get 'faq', to: 'basic_informations#faq'
  get 'privasi', to: 'basic_informations#privacy_policy'
  get 'carakerja', to: 'basic_informations#how_it_work'
end